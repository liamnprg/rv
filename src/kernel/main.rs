/* diosix high-level kernel main entry code
 *
 * (c) Chris Williams, 2018.
 *
 * See LICENSE for usage and copying.
 */

#![feature(int_to_from_bytes)]
#![feature(asm)]
#![no_std]
#![no_main]

use core::panic::PanicInfo;
use core::char;
use core::str;

#[no_mangle]
pub extern "C" fn kmain() -> !  {
    let mut r: usize = 0x34;
    serial_write2(r.to_be_bytes());

 //   unsafe {
        /*asm!("csrr a1,0x300
             lui a1,%hi(0x10013000)
             sw 0x61, 0(a1)": : : :);*/
//        asm!("lui $0,$1":"=&r"(r) :"r"(r):: "volatile");
  //  }
//    serial_write2(r.to_le_bytes());
    panic!();
}
fn serial_write2(s: [u8;4]) {
  for c in s.iter() {
    let uart_tx = 0x10013000 as *mut char;
    unsafe { *uart_tx = *c as char; }
  }
}

fn serial_write(s: &str) {
  for c in s.chars() {
    let uart_tx = 0x10013000 as *mut char;
    unsafe { *uart_tx = c; }
  }
}

/* we're on our own here, so we need to provide these */
#[panic_handler]
#[no_mangle]
pub fn panic(_info: &PanicInfo) -> !  {
    serial_write("murder death kill\n");
    loop {}
}

#[no_mangle]
pub extern "C" fn abort() -> !  {
  serial_write("aborted");
  loop {}
}
