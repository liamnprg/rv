all:
	./build.sh --triple riscv32imac-unknown-none-elf --platform sifive_e

run:
	qemu-system-riscv32 -machine sifive_e -kernel target/riscv32imac-unknown-none-elf/release/kernel -nographic
e:
	vi src/kernel/main.rs
